export enum attendenceStatus {
  PRESENT = 0,
  ABSCENT = 1,
  ON_LEAVE = 2,
  UNDEFINED = 3
}
