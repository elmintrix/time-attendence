export interface day {
    date: string,
    dayOfMonth: number
    isCurrentMonth: boolean,
    isCurrentDay: boolean
    attendenceStatus: string
    isAfterCurrentDate: boolean
    visibleModal: boolean;
}