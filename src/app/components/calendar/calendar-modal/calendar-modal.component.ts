import { Component, OnInit } from '@angular/core';
import { Input } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { day } from 'src/app/utils/types';
@Component({
  selector: 'app-calendar-modal',
  templateUrl: './calendar-modal.component.html',
  styleUrls: ['./calendar-modal.component.css'],
})
export class CalendarModalComponent implements OnInit {

  radioBtnSelectedStatus = null;
  radioBtnStatuses = [
    { name: 'Present', key: 'P' },
    { name: 'Abscent', key: 'A' },
    { name: 'Leave', key: 'L' },
    { name: 'Other', key: 'O' },
  ];

  @Input() day!: day;
  formGroup!: FormGroup;

  ngOnInit(): void {
    this.formGroup = new FormGroup({
      date: new FormControl(this.day.date),
      selectedStatus: new FormControl('',Validators.required),
      justification: new FormControl('',Validators.required)
    });
  }

  concealModal() {
    this.day.visibleModal = false;
  }

  submit() {

    if(this.formGroup.valid){
      this.concealModal();
      // process and send it to the backend
    }else {
      console.log('invalid form')
    }
    
  }
}
