import { Component,ViewChild } from '@angular/core';
import * as dayjs from 'dayjs';
import * as weekday from 'dayjs/plugin/weekday';
import * as weekOfYear from 'dayjs/plugin/weekOfYear';
import { day } from 'src/app/utils/types';
import { CalendarModalComponent } from './calendar-modal/calendar-modal.component';

dayjs.extend(weekday);
dayjs.extend(weekOfYear);

@Component({
  selector: 'app-calendar',
  templateUrl: './calendar.component.html',
  styleUrls: ['./calendar.component.css'],
})
export class CalendarComponent {

  currentDay = dayjs().format('YYYY-MM-DD');
  weekdays: string[] = ['Sun', 'Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat'];
  attendanceStatuses = ['present', 'abscent', 'leave', 'undefined'];
  todayDate: string = dayjs().format('YYYY-MM-DD');
  selectedMonth: dayjs.Dayjs = dayjs();
  allDays: day[] = [];

  @ViewChild(CalendarModalComponent) modal!:CalendarModalComponent ;

  constructor() {
    this.createCalendar();
  }

  createCalendar(year = dayjs().format('YYYY'), month = dayjs().format('M')) {
    this.selectedMonth = dayjs(`${year}-${month}-01`);
    const currentMonthDays = this.createDaysForCurrentMonth(year, month);
    const previousMonthDays = this.createDaysForPreviousMonth();
    const nextMonthDays = this.createDaysForNextMonth(year, month);

    this.allDays = [
      ...previousMonthDays,
      ...currentMonthDays,
      ...nextMonthDays,
    ];
  }

  createDaysForCurrentMonth(year: string, month: string): day[] {
    const daysInMonth = dayjs(`${year}-${month}-01`).daysInMonth();

    return [...Array(daysInMonth)].map((_, index) => {
      const date = dayjs(`${year}-${month}-${index + 1}`).format('YYYY-MM-DD');
      return {
        date: date,
        dayOfMonth: index + 1,
        isCurrentMonth: true,
        isCurrentDay: this.currentDay === date ? true : false,
        attendenceStatus: 'present', // hardcoded need proper attendant algorithem
        visibleModal: false,
        isAfterCurrentDate: dayjs().isAfter(date),
      };
    });
  }

  createDaysForPreviousMonth(): day[] {
    const firstDayOfTheMonthWeekday = this.getWeekday(
      this.selectedMonth.format('YYYY-MM-01')
    );
    const previousMonth = this.selectedMonth.subtract(0, 'month');
    const visibleNumberOfDaysFromPreviousMonth = firstDayOfTheMonthWeekday
      ? firstDayOfTheMonthWeekday
      : 6;
    const previousMonthLastMondayDayOfMonth =
      this.selectedMonth.subtract(0, 'month').endOf('month').date() -
      visibleNumberOfDaysFromPreviousMonth;

    return [...Array(visibleNumberOfDaysFromPreviousMonth)].map((_, index) => {
      const date = dayjs(
        `${previousMonth.year()}-${previousMonth.month()}-${
          previousMonthLastMondayDayOfMonth + index
        }`
      ).format('YYYY-MM-DD');
      return {
        date: date,
        dayOfMonth: previousMonthLastMondayDayOfMonth + index,
        isCurrentMonth: false,
        isCurrentDay: false,
        visibleModal: false,
        attendenceStatus: 'abscent', // hardcoded need proper attendant algorithem
        isAfterCurrentDate: dayjs().isAfter(date),
      };
    });
  }

  createDaysForNextMonth(year: string, month: string): day[] {
    const daysInMonth = this.selectedMonth.daysInMonth();
    const lastDayOfTheMonthWeekday = this.getWeekday(
      `${year}-${month}-${daysInMonth}`
    );
    const nextMonth = this.selectedMonth.add(1, 'month');
    const visibleNumberOfDaysFromNextMonth = lastDayOfTheMonthWeekday
      ? 7 - lastDayOfTheMonthWeekday
      : 0;

    return [...Array(visibleNumberOfDaysFromNextMonth)].map((_, index) => {
      const date = dayjs(
        `${nextMonth.year()}-${nextMonth.month() + 1}-${index + 1}`
      ).format('YYYY-MM-DD');
      return {
        date: date,
        dayOfMonth: index + 1,
        isCurrentMonth: false,
        isCurrentDay: false,
        visibleModal: false,
        attendenceStatus: '', // hardcoded need proper attendant algorithem
        isAfterCurrentDate: dayjs().isAfter(date),
      };
    });
  }
  getWeekday(date: string) {
    return dayjs(date).weekday();
  }

  previousMonth() {
    this.selectedMonth = this.selectedMonth.subtract(1, 'month');
    this.createCalendar(
      this.selectedMonth.format('YYYY'),
      this.selectedMonth.format('M')
    );
  }

  today() {
    this.selectedMonth = dayjs();
    this.createCalendar(
      this.selectedMonth.format('YYYY'),
      this.selectedMonth.format('M')
    );
  }

  nextMonth() {
    this.selectedMonth = this.selectedMonth.add(1, 'month');
    this.createCalendar(
      this.selectedMonth.format('YYYY'),
      this.selectedMonth.format('M')
    );
  }

  getAttendanceStatus(attendenceStatus: string): string | undefined {
    if (attendenceStatus === 'present') {
      return 'success';
    } else if (attendenceStatus === 'abscent') {
      return 'danger';
    } else if (attendenceStatus === 'leave') {
      return 'warning';
    }
    return undefined;
  }

  showModal(day: day, event: Event){
    day.visibleModal = true 
    event.stopPropagation()
  }
}
